import { ref } from "vue";
import { auth, onAuthStateChanged } from "@/configs/firebase";

const user = ref(auth.currentUser);
onAuthStateChanged(auth, (responseUser) => {
  if (responseUser) {
    user.value = responseUser;
  }
});

function getUser() {
  return {
    user,
  };
}
export function useUser() {
  return { getUser };
}
