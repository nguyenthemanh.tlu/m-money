import { ref } from "vue";
import { auth, signOut } from "@/configs/firebase";

const error = ref(null);

async function logout() {
  try {
    const response = await signOut(auth);

    return response;
  } catch (err) {
    error.value = err.message;
  }
}

export function useLogout() {
  return { error, logout };
}
