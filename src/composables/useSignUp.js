import { ref } from "vue";
import {
  auth,
  createUserWithEmailAndPassword,
  updateProfile,
} from "@/configs/firebase";

const error = ref(null);
const isPending = ref(false);
async function signUp(email, password, fullName) {
  console.log("use sign up");
  try {
    isPending.value = true;
    error.value = null;
    const response = await createUserWithEmailAndPassword(
      auth,
      email,
      password
    );
    await updateProfile(response.user, { displayName: fullName });

    if (!response) throw new Error("Could not create a new user");

    return response;
  } catch (err) {
    error.value = err.message;
  } finally {
    isPending.value = false;
  }
}

export function useSignUp() {
  return { error, isPending, signUp };
}
