import { ref } from "vue";
import { auth, signInWithEmailAndPassword } from "@/configs/firebase";
const error = ref(null);
const isPending = ref(null);
async function signIn(email, password) {
  try {
    isPending.value = true;
    const response = await signInWithEmailAndPassword(auth, email, password);

    return response;
  } catch (err) {
    error.value = err.message;
  } finally {
    isPending.value = false;
  }
}

export function useSignIn() {
  return { error, isPending, signIn };
}
