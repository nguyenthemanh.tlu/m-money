// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore, serverTimestamp } from "firebase/firestore";
import {
  getAuth,
  createUserWithEmailAndPassword,
  updateProfile,
  signInWithEmailAndPassword,
  onAuthStateChanged,
  signOut,
} from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyApD0Ec_PEsUbmGiwfvhyVmlt3C9dNWcWE",
  authDomain: "mmoney-8f4f5.firebaseapp.com",
  projectId: "mmoney-8f4f5",
  storageBucket: "mmoney-8f4f5.appspot.com",
  messagingSenderId: "598135526009",
  appId: "1:598135526009:web:21846594f94f21839f7aa8",
};

// Initialize Firebase
initializeApp(firebaseConfig);

const firestore = getFirestore();
const auth = getAuth();
// const timestamp = firestore.firestore.FieldValue.timestamp;

export {
  auth,
  firestore,
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  serverTimestamp,
  updateProfile,
  onAuthStateChanged,
  signOut,
};
