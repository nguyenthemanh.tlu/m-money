export const PUBLIC_LAYOUT = "default";

export const PROFILES_OPTIONS = [
  {
    text: "My Wallet",
    icon: "t2ico-wallet",
    route: {
      name: "Home",
      params: {},
    },
  },
  {
    text: "Tools",
    icon: "t2ico-ticket-star",
    route: {
      name: "Home",
      params: {},
    },
  },
  {
    text: "Privacy",
    icon: "t2ico-lock",
    route: {
      name: "Home",
      params: {},
    },
  },
  {
    text: "About",
    icon: "t2ico-info-square",
    route: {
      name: "Home",
      params: {},
    },
  },
  {
    text: "Logout",
    icon: "t2ico-logout",
    route: {
      name: "Logout",
      params: {},
    },
  },
];
