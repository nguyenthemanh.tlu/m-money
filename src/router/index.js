import { createRouter, createWebHistory } from "vue-router";
import { auth } from "@/configs/firebase";

import register from "../views/register.vue";
import login from "../views/login.vue";
import index from "../views/index.vue";
import profile from "../views/profile.vue";
import logout from "../views/logout.vue";

const requiredAuth = (to, from, next) => {
  const authUser = auth.currentUser;
  if (!authUser) {
    next({ name: "login", params: {} });
    return;
  } else {
    next();
  }
};

const routes = [
  {
    path: "/register",
    name: "register",
    component: register,
    meta: {
      layout: "auth",
    },
  },
  {
    path: "/login",
    name: "login",
    component: login,
    meta: {
      layout: "auth",
    },
  },
  {
    path: "/home",
    name: "Home",
    component: index,
    beforeEnter: requiredAuth,
  },
  {
    path: "/profile",
    name: "Profile",
    component: profile,
    beforeEnter: requiredAuth,
  },
  {
    path: "/logout",
    name: "Logout",
    component: logout,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
