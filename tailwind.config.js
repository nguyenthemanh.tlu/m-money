module.exports = {
  content: [],
  theme: {
    extend: {
      colors: {
        dark: "#94a3b8",
        primary: "#e11d48",
        green: "#14b8a6",
        red: "#e11d48",
        "dark-light": "#F8FAFB",
      },
    },
  },
  plugins: [],
};
